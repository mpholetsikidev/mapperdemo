package co.za.mpholetsiki.api.user;

import java.util.Date;

public class UserDto {

    private Long id;
    private Date createdOn = new Date();
    private Date updatedOn = new Date();
    private UserDto createdBy;
    private UserDto updatedBy;
    private String role;
    private String name;
    private String surname;
    private String email;
    private String username;
    private String password;

    public UserDto() {
    }

    public Long getId() {
        return id;
    }

    public UserDto setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public UserDto setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public UserDto setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public UserDto getCreatedBy() {
        return createdBy;
    }

    public UserDto setCreatedBy(UserDto createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public UserDto getUpdatedBy() {
        return updatedBy;
    }

    public UserDto setUpdatedBy(UserDto updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public String getRole() {
        return role;
    }

    public UserDto setRole(String role) {
        this.role = role;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public UserDto setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDto setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto that = (UserDto) o;
        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(id, that.id)
                .append(createdOn, that.createdOn)
                .append(updatedOn, that.updatedOn)
                .append(createdBy, that.createdBy)
                .append(updatedBy, that.updatedBy)
                .append(role, that.role)
                .append(name, that.name)
                .append(surname, that.surname)
                .append(email, that.email)
                .append(username, that.username)
                .append(password, that.password)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(id)
                .append(createdOn)
                .append(updatedOn)
                .append(createdBy)
                .append(updatedBy)
                .append(role)
                .append(name)
                .append(surname)
                .append(email)
                .append(username)
                .append(password)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", role='" + role + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
