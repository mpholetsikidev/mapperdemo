package co.za.mpholetsiki.api.user;

import co.za.mpholetsiki.data.user.UserEntity;
import co.za.mpholetsiki.data.user.AbstractUserService;
import co.za.mpholetsiki.mapper.UserMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserController {

    @Resource(name = "systemUserService")
    AbstractUserService systemUserService;

//	@Autowired
//	UserMapper userMapper;

    @RequestMapping(value = "/mapstruct-demo/user", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN') AND hasRole('USER')")
    public ResponseEntity<UserDto> getOneUser(final @RequestParam(value = "username") String username,
                                              final @RequestParam(value = "password") String password) {
        UserEntity ent = new UserEntity();
        ent.setName("Mpho");
        ent.setSurname("Letsiki");
//        systemUserService.get(username, password);
        UserDto dto = UserMapper.INSTANCE.transfer(ent);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
