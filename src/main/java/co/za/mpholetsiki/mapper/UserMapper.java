package co.za.mpholetsiki.mapper;

import co.za.mpholetsiki.data.user.UserEntity;
import co.za.mpholetsiki.api.user.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
//				(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto transfer(UserEntity entity);

    UserEntity transfer(UserDto dto);

//	List<UserDto> transfer(List<UserEntity> userEntities);

//	List<?> transfer(List<?> list);
}
