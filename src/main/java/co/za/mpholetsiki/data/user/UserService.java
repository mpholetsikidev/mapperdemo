package co.za.mpholetsiki.data.user;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("systemUserService")
public class UserService extends AbstractUserService {

    @Resource
    UserRepository systemUserRepository;

    @Override
    public UserEntity get(final String username, final String password) {
        return systemUserRepository.findByUsernameAndPassword(username, password);
    }
}
