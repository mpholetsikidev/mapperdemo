package co.za.mpholetsiki.data.user;

import java.util.Date;

//@Entity
//@Table(name = "system_user")
public class UserEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
    private Long id;

//    @Column(name = "created", updatable = false)
//    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date();

//    @Column(name = "updated")
//    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn = new Date();

//    @ManyToOne
//    @JoinColumn(name = "created_by", referencedColumnName = "id")
    private UserEntity createdBy;

//    @ManyToOne
//    @JoinColumn(name = "updated_by", referencedColumnName = "id")
    private UserEntity updatedBy;

//    @Column(name = "role")
    private String role;

//    @Column(name = "name")
    private String name;

//    @Column(name = "surname")
    private String surname;

//    @Column(name = "email", updatable = false)
    private String email;

//    @Column(name = "username", updatable = false)
    private String username;

//    @Column(name = "password")
    private String password;

    public UserEntity() {
    }

    public Long getId() {
        return id;
    }

    public UserEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public UserEntity setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public UserEntity setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public UserEntity getCreatedBy() {
        return createdBy;
    }

    public UserEntity setCreatedBy(UserEntity createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public UserEntity getUpdatedBy() {
        return updatedBy;
    }

    public UserEntity setUpdatedBy(UserEntity updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public String getRole() {
        return role;
    }

    public UserEntity setRole(String role) {
        this.role = role;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public UserEntity setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserEntity setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserEntity setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(id, that.id)
                .append(createdOn, that.createdOn)
                .append(updatedOn, that.updatedOn)
                .append(createdBy, that.createdBy)
                .append(updatedBy, that.updatedBy)
                .append(role, that.role)
                .append(name, that.name)
                .append(surname, that.surname)
                .append(email, that.email)
                .append(username, that.username)
                .append(password, that.password)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(id)
                .append(createdOn)
                .append(updatedOn)
                .append(createdBy)
                .append(updatedBy)
                .append(role)
                .append(name)
                .append(surname)
                .append(email)
                .append(username)
                .append(password)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", role='" + role + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
