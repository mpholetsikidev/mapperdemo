package co.za.mpholetsiki.data.user;

public abstract class AbstractUserService {

    public abstract UserEntity get(final String username, final String password);
}
